const fs = require('fs');
const path = require('path');

function createAndDelete (num, dummyData) {

    let dummyInJSON = JSON.stringify(dummyData);

    fs.mkdir(path.join(__dirname,"Folder"),(err)=>{
        if(err) {
            console.log(err);
        } else {
            console.log("directory successfully created")
        }
    })

    let filename = "file";

    for (let count = 0 ; count < num ; count++) {

        fs.writeFile(path.join('./Folder',filename+count+".json"), dummyInJSON , (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(filename+count+".json", "successfully written");
                fs.unlink(path.join('./Folder',filename+count+".json"), (err)=>{
                    if (err) {
                        console.log(err)
                    } else {
                        console.log(filename+count+".json", " was successfully deleted");
                    }
                })
            }
        })
    }

    
}

module.exports = createAndDelete;