const fs = require('fs');
const path = require('path');

function tasks () {
    fs.readFile("lipsum.txt","utf-8",(err,data)=>{                                              // Instruction 1
        if (err) {
            console.log(err,"could not read lipsum file")
        } else {
            let lipsumData = data;
            let convertedToUpper = lipsumData.toUpperCase();
            fs.writeFile("upper.txt",convertedToUpper, (err)=> {
                if (err) {
                    console.log(err, "upper could not be written");
                } else {
                    fs.writeFile("filenames.txt","upper.txt", (err)=> {
                        if (err) {
                            console.log(err);
                        } else {
                            fs.readFile("upper.txt","utf-8",(err,data)=>{
                                if (err) {
                                    console.log(err);
                                } else {
                                    let readData = data;
                                    let lowerData = readData.toLowerCase();
                                    let splittedData = lowerData.split(/[.\n]/);
                                    for (let i = 0; i< splittedData.length ; i++) {
                                        splittedData[i]=splittedData[i].trim();
                                    }
                                    splittedData = JSON.stringify(splittedData);
                                    fs.writeFile("splitted.json",splittedData,(err)=>{
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            fs.appendFile("filenames.txt",",splitted.json",(err)=> {
                                                if (err) {
                                                    console.log(err);
                                                } else {
                                                    fs.readFile("splitted.json","utf-8",(err,data)=>{
                                                        if(err) {
                                                            console.log(err);
                                                        } else {
                                                            let sentenceArray = JSON.parse(splittedData);
                                                            sentenceArray.sort();
                                                            let sortedData = sentenceArray.join("");
                                                            fs.writeFile("sorted.txt",sortedData,(err)=>{
                                                                if (err) {
                                                                    console.log(err)
                                                                } else {
                                                                    fs.appendFile("filenames.txt",",sorted.txt", (err)=>{
                                                                        if (err) {
                                                                            console.log(err);
                                                                        } else {
                                                                            fs.readFile("filenames.txt","utf-8",(err,data)=>{
                                                                                if(err) {
                                                                                    console.log(err);
                                                                                } else {
                                                                                    let filenameData = data;
                                                                                    let namesOfFiles = filenameData.split(",");
                                                                                    for (let i = 0 ; i < namesOfFiles.length ; i++) {
                                                                                        fs.unlink(namesOfFiles[i],(err)=>{
                                                                                            if (err) {
                                                                                                console.log(err)
                                                                                            } else {
                                                                                                console.log("file removed successfully");
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = tasks;